<?php
$writer = new XMLWriter();
$writer ->openMemory();
$writer ->setIndent(true);
$writer ->setIndentString("     ");
$writer ->startDocument("1.0","UTF-8");

$writer ->startElement('students');

$writer ->startElement('student');

$writer ->writeElement("Name","อภิวัฒน์");

$writer ->startElement('books');
$writer ->writeElement("book","Don't make me think");
$writer ->writeElement("book","On writing well");
$writer ->writeElement("book","The craft of research");
$writer ->endElement();

$writer ->startElement('education');
$writer ->writeElement("highSchool","Ayutthaya witthayalai");
$writer ->writeElement("undergradSchool","Khon Kaen University");
$writer ->endElement();

$writer ->endElement();

$writer ->startElement('student');

$writer ->writeElement("Name","ชนพัฒน์");

$writer ->startElement('books');
$writer ->writeElement("book","ภาพปริศนา");
$writer ->writeElement("book","พระพุทธเจ้า");
$writer ->writeElement("book","หมากกระดาน");
$writer ->endElement();

$writer ->startElement('education');
$writer ->writeElement("highSchool","Satit KKU");
$writer ->writeElement("undergradSchool","Khon Kaen University");
$writer ->endElement();



$writer ->endDocument();

header('Content-type:text/xml');
echo $writer ->outputMemory();

?>