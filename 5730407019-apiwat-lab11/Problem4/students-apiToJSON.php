<?php

$CreateAr = array(
    "students" => array(
        array("name" => "apiwat",
              "books" => array("Don't make me think" 
                               ,"On writing well"
                               ,"The craft of research"),
              "education" => array(
                        array("high school" => "Ayutthaya witthayalai school"),
                        array("undergrad school" => "Khon Kaen University"))
              ),
         array("name" => "Asanee",
              "books" => array("ภาพปริศนา" 
                               ,"พระพุทธเจ้า"
                               ,"หมากกระดาน"),
              "education" => array(
                        array("high school" => "Satit KKU"),
                        array("undergrad school" => "Khon Kaen University")
              )))           
    );

$json = json_encode($CreateAr,JSON_UNESCAPED_UNICODE);
header('Content-Type: application/json;charset = UTF-8');

echo $json;
?>